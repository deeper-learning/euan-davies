<?php
    require 'vendor/autoload.php';

    use Carbon\Carbon;

    // printf("Now: %s", Carbon::now());

    $carbon = new Carbon();
    $isUploadSuccessful = null;
class Form1
{
    public $name;
    public $dob;
    public $email;
    public $picture;
    public $submitted;
}

if (!empty($_POST)) {
    $form1 = new Form1();
    $form1->name = $_POST['name'];
    $form1->dob = $_POST['dob'];
    $form1->email = $_POST['email'];
    $form1->submitted = $carbon;
//    var_dump($form1);
    $serialisedform1 = serialize($form1);
    file_put_contents($form1->submitted.'.txt',$serialisedform1 . PHP_EOL, FILE_APPEND);

//    $message = 'Form Created Successfully; ';
//    $message .= ' Name: ' . $form1->name;
//    $message .= ' , Email: ' . $form1->email;
//    $message .= ' , Date of Birth: ' . $form1->dob;
//    $message .= ' , Time of Submission: ' . $form1->submitted;
//    echo $message;
//    die();
}

if (!empty($_FILES)) {

//    var_dump($_FILES);
    if (!empty($_FILES['profilePicture'])) {
        $file = $_FILES['profilePicture'];
        if ($file['error'] !== UPLOAD_ERR_OK) {
        }

        $targetPath = 'uploads/' . time() . '_' . $file['name'];
        if (!move_uploaded_file(
            $file['tmp_name'],
            $targetPath
        )) {
            $isUploadSuccessful = false;
//            throw new RuntimeException('Failed to move the file');
        } else {
            $form1->picture = $targetPath;
            $isUploadSuccessful = true;
        }
    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <title>Hello, world!</title>
</head>
<body>
<div>
    <?php if($isUploadSuccessful === true): ?>
        <div class="alert alert-success" id="message">Form has been submitted successfully.</div>
    <?php elseif ($isUploadSuccessful === false): ?>
        <div class="alert alert-danger" id="danger">Form failed.</div>
    <?php endif; ?>
    <form action="" method="post" enctype="multipart/form-data" id="form1">
        <div class="form-group">
            <label for="exampleFormControlInput1">Name</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="input name">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Email Address</label>
            <input type="text" class="form-control" name="email" id="email" placeholder="name@example.com">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Date of Birth</label>
            <input type="date" class="form-control" name="dob" id="dob">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Upload Picture</label>
            <div class="custom-file">
                <input type="file" class="form-control-file" name="profilePicture" id="profile-pic">
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" id="carousel-inner">
            <?php
                $images = array_diff(scandir('uploads/', SCANDIR_SORT_NONE), array('..', '.'));
                $head = $images[0];
                echo "<div class='carousel-item active'>";
                echo "<img src='uploads/$head' class='d-block w-100'>";
                echo "</div>";
                function getTail($i)
                {
                    return $i > 0;
                }

                $tail = array_filter($images, 'getTail', ARRAY_FILTER_USE_KEY);
                foreach ($tail as $image) {
                    echo "<div class='carousel-item'>";
                    echo "<img src='uploads/$image' class='d-block w-100'>";
                    echo "</div>";
                };
            ?>
        </div>
    </div>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
    //
    // function form1() {
    //     axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    //
    //     var name = $('#name').val();
    //     var email = $('#email').val();
    //     var dob = $('#dob').val();
    //
    //     var data = new FormData();
    //     data.append('name', name);
    //     data.append('email', email);
    //     data.append('dob', dob);
    //
    //     axios.post('http://localhost/exercises/week-04/workbook/project.php', data)
    //         .then(function (response) {
    //             console.log(response);
    //             $('#message').removeClass('invisible').text(response.data);
    //         })
    //         .catch(function(error) {
    //             console.log(error);
    //         });
    // }
</script>
</html>


