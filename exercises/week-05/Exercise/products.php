<?php

require_once 'db.php';

if (!isset($_GET['id'])) {
    die('Missing id in URL');
}

class Product
{
    public int $id;
    public string $title;
}

$stmt = $dbh->prepare(
    'SELECT * FROM products WHERE id = :id'
);
$stmt->execute(['id' => $_GET['id']]);

$product = $stmt->fetchObject(Product::class);
//$product = $stmt->fetchAll(PDO::FETCH_CLASS, Product::class);
var_dump($product);

?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
</head>
<body>
    <div>
    </div>
</body>
</html>