<?php

class CheckIn
{
    public $Name;
    public $Rating;
    public $Review;
    public $Timestamp;
}

if (!empty($_POST)) {
    $checkIn = new CheckIn();
    $checkIn->Name = $_POST['name'];
    $checkIn->Rating = $_POST['rating'];
    $checkIn->Review = $_POST['review'];
    $checkIn->Timestamp = date('F d, Y h:i:s a.', time());
    $serialisedCheckIn = serialize($checkIn);
    file_put_contents('my-checkIn.txt',$serialisedCheckIn . PHP_EOL, FILE_APPEND);

    $message = 'Review Created; ';
    $message .= ' Name: ' . $checkIn->Name;
    $message .= ' , Rating: ' . $checkIn->Rating;
    $message .= ' , Review: ' . $checkIn->Review;
    $message .= ' , Reload the page to see your review!';
    echo $message;
    die();
}

?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <style>
        .star-rating {
            background-color: grey;
            width: 200px;
            height: 30px;
            display: inline-block;
        }

        .star-rating div {
            height: 100%;
            background-color: cornflowerblue;

        }
    </style>
    <title>Hello, world!</title>
</head>
<body>
    <div class="row m-4" id="main">
        <div class="row border p-2 pb-4 m-3">
           <div class="col-md-12 col-xl-6">
               <div class="card-body">
                   <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                       <div class="carousel-inner">
                           <div class="carousel-item active">
                               <img class="d-block w-100" src="Cat1.jpg" alt="First slide">
                           </div>
                           <div class="carousel-item">
                               <img class="d-block w-100" src="cat2.png" alt="Second slide">
                           </div>
                           <div class="carousel-item">
                               <img class="d-block w-100" src="cat2.png" alt="Third slide">
                           </div>
                           <div class="carousel-item">
                               <img class="d-block w-100" src="cat2.png" alt="Fourth slide">
                           </div>
                       </div>
                       <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="sr-only">Previous</span>
                       </a>
                       <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                           <span class="carousel-control-next-icon" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                       </a>
                   </div>
               </div>
           </div>
            <div class="col-md-12 col-xl-6">
                <div class="card-body">
                    <h1 class="card-title">Lorem Ipsum</h1>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#checkinForm">Check In</button>
                    <div class="alert alert-success invisible " id="message"></div>
                <div>
                <div class="modal fade" id="checkinForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form action="" method="post" id="form1">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Submit Your Review</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Name</label>
                                        <input type="text" class="form-control" name="Name" id="Name" placeholder="Enter Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Rating</label>
                                        <select class="form-control" name="Rating" id="Rating">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Review</label>
                                        <input type="text" class="form-control" name="Review" id="Review" aria-describedby="emailHelp" placeholder="Enter Review">
                                    </div>
                            </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
        <div class="col-12">
            <br>
            <h2 class="col-12">Additional Information</h2>
            <br>
            <div class="card col-12">
                <table class="table">
                    <tbody>
                    <br>
                    <tr>
                        <th scope="row">Average Rating</th>
                        <td><img src="stars.png"></td>
                    </tr>
                    <tr>
                        <th scope="row">Another Statistic</th>
                        <td>78/100</td>
                    </tr>
                    <tr>
                        <th scope="row">Yet Another Statistic</th>
                        <td>Something</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <br>
        </div>
        <div class="col-12">
            <br>
            <h2 class="col-12">Recent Checkins</h2>
            <br>
                <?php
                foreach ($checkIns as $line):
                    // unserialize content
                    $checkIn = unserialize($line);
                    ?>
                <div class="card col-12 p-4" id="checkins">
                    <h3><?=$checkIn->Name; ?></h3>
                    <p class="card-text"><?=$checkIn->Rating; ?></p>
                    <p><?=$checkIn->Review; ?></p>
                </div>
                <?php endforeach; ?>
        </div>
    </div>
</body>
<script>
    window.addEventListener('DOMContentLoaded', function() {
        var form = $('#form1');
        form.on('submit', function(e) {
            e.preventDefault();
            checkin();
        });
    });

    function checkin() {
        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

        var name = $('#Name').val();
        var rating = $('#Rating').val();
        var review = $('#Review').val();

        var data = new FormData();
        data.append('name', name);
        data.append('rating', rating);
        data.append('review', review);

        axios.post('http://localhost/project/Week%203/Project/Project%20Week%203.php', data)
            .then(function (response) {
                console.log(response);
                $('#message').removeClass('invisible').text(response.data);
            })
            .catch(function(error) {
                console.log(error);
            });
    }
</script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</html>