<?php
//define('SITE_KEY', '6Lecxl4aAAAAADDPSuX8qxl052DfjluPUX5KUTTa');
//define('SECRET_KEY','6Lecxl4aAAAAAOylycU89TNHPcsj-HHnMVSos8c4');
//
//if($_POST){
//    function getCaptcha(SECRET_KEY) {
//        $Response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".SECRET_KEY."&response=".SECRET_KEY."");
//        $Return = json_decode($Response);
//        return $Return;
//        }
//    $Return = getCaptcha($_POST['g-recaptcha-response']);
//    var_dump($Return);
//}
//
//$recaptcha = new \ReCaptcha\ReCaptcha($secret);
//$resp = $recaptcha->setExpectedHostname('http://localhost/project/Week%205/W5S1_Project/Project%20Week%205.php')
//    ->setExpectedAction('homepage')
//    ->setScoreThreshold(0.5)
//    ->verify($gRecaptchaResponse, $remoteIp);
//
//if ($resp->isSuccess()) {
//    echo 'Verified!';
//} else {
//    $errors = $resp->getErrorCodes();
//}

require_once __DIR__ . '/vendor/autoload.php';

if (isset($_POST['g-recaptcha-response'])) {
    $secret = '6LdM-mQaAAAAACCnrukf4-zB_Ou_smz52nYTZ4OG';
    $gRecaptchaResponse = $_POST['g-recaptcha-response'];
    $remoteIp = $_SERVER['REMOTE_ADDR'];
    var_dump($remoteIp);
    $recaptcha = new \ReCaptcha\ReCaptcha($secret);
    $resp = $recaptcha->setExpectedHostname('localhost')
        ->verify($gRecaptchaResponse, $remoteIp);
    if ($resp->isSuccess()) {
        echo 'Verified!';
    } else {
        $errors = $resp->getErrorCodes();
        var_dump($errors);
    }
}

require_once 'db.php';

$checkinAddedSuccess = false;

class CheckIn
{
    public $user_name;
    public $rating;
    public $review;
    public $submitted;
}

if (!empty($_POST)) {
//    $checkIn = new CheckIn();
//    $checkIn->name = $_POST['name'];
//    $checkIn->rating = $_POST['rating'];
//    $checkIn->review = $_POST['review'];
//    $checkIn->timestamp = date('F d, Y h:i:s a.', time());
//    $serialisedCheckIn = serialize($checkIn);
//    file_put_contents('my-checkIn.txt',$serialisedCheckIn . PHP_EOL, FILE_APPEND);

    $stmt = $dbh->prepare(
        'INSERT INTO checkins (user_name, rating, review) VALUES (:user_name, :rating, :review)'
    );

    $stmt->execute([
        'user_name' => $_POST['Name'],
        'rating' => $_POST['Rating'],
        'review' => $_POST['Review'],
    ]);

    if ($stmt->rowCount() > 0) {
            $checkinAddedSuccess = true;
    }

}

$stmt = $dbh->prepare(
    'SELECT id, user_name, rating, review, submitted FROM project.checkins'
);
$stmt->execute(
);

$checkInArrays = $stmt->fetchAll(PDO::FETCH_ASSOC);
$checkInArray = null;


// var_dump($checkInArrays);



?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<!--    <script src="https://www.google.com/recaptcha/api.js?render=--><?php //echo SITE_KEY; ?><!--"></script>-->
<!--    <script src="https://www.google.com/recaptcha/api.js"></script>-->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <style>
        .star-rating {
            background-color: grey;
            width: 200px;
            height: 30px;
            display: inline-block;
        }

        .star-rating div {
            height: 100%;
            background-color: cornflowerblue;

        }
    </style>
    <title>Hello, world!</title>
</head>
<body>
    <div class="row m-4" id="main">
        <div class="row border p-2 pb-4 m-3">
           <div class="col-md-12 col-xl-6">
               <div class="card-body">
                   <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                       <div class="carousel-inner">
                           <div class="carousel-item active">
                               <img class="d-block w-100" src="Cat1.jpg" alt="First slide">
                           </div>
                           <div class="carousel-item">
                               <img class="d-block w-100" src="cat2.png" alt="Second slide">
                           </div>
                           <div class="carousel-item">
                               <img class="d-block w-100" src="cat2.png" alt="Third slide">
                           </div>
                           <div class="carousel-item">
                               <img class="d-block w-100" src="cat2.png" alt="Fourth slide">
                           </div>
                       </div>
                       <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                           <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                           <span class="sr-only">Previous</span>
                       </a>
                       <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                           <span class="carousel-control-next-icon" aria-hidden="true"></span>
                           <span class="sr-only">Next</span>
                       </a>
                   </div>
               </div>
           </div>
            <div class="col-md-12 col-xl-6">
                <div class="card-body">
                    <h1 class="card-title">Lorem Ipsum</h1>
                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#checkinForm">Check In</button>
                    <div class="alert alert-success invisible " id="message"></div>
                <div>
                <div class="modal fade" id="checkinForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form action="" method="post" id="form1">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Submit Your Review</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                    <div class="form-group">
                                        <label for="checkin-name">Name</label>
                                        <input type="text" class="form-control" name="Name" id="Name" placeholder="Enter Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="checkin-rating">Rating</label>
                                        <select class="form-control" name="Rating" id="Rating">
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="checkin-review">Review</label>
                                        <input type="text" class="form-control" name="Review" id="Review" aria-describedby="emailHelp" placeholder="Enter Review">
                                    </div>
                                        <div class="g-recaptcha" data-sitekey="6LdM-mQaAAAAAFeVF1ph_NDJSPLUQkHSgqYZvMQi"></div>
                                        <br/>
                            </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<!--                                    <input type="text" id="g-recaptcha-response" name="g-recaptcha-response">-->
<!--                                    <button type="submit" class="btn btn-primary g-recaptcha"-->
<!--                                        data-sitekey="--><?php //SITE_KEY; ?><!--" data-callback='onSubmit' data-action='submit'-->
<!--                                    >Save changes</button>-->
                                    <input type="submit" value="Submit">
                                </div>
                            </form>
                            <script>

                                //function onClick(e) {
                                //    e.preventDefault();
                                //    grecaptcha.ready(function() {
                                //        grecaptcha.execute('<?php //echo SITE_KEY; ?>//', {action: 'submit'}).then(function(token) {
                                //            console.log(token);
                                //            //document.getElementById('g-recaptcha-response').value=token;
                                //        });
                                //    });
                                //}
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
        <div class="col-12">
            <br>
            <h2 class="col-12">Additional Information</h2>
            <br>
            <div class="card col-12">
                <table class="table">
                    <tbody>
                    <br>
                    <tr>
                        <th scope="row">Average Rating</th>
                        <td><img src="stars.png"></td>
                    </tr>
                    <tr>
                        <th scope="row">Another Statistic</th>
                        <td>78/100</td>
                    </tr>
                    <tr>
                        <th scope="row">Yet Another Statistic</th>
                        <td>Something</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <br>
        </div>
        <div class="col-12">
            <br>
            <h2 class="col-12">Recent Checkins</h2>
            <br>
            <?php if ($checkinAddedSuccess): ?>
                <p class="alert alert-success">Your Review has been added successfully.</p>
            <?php endif; ?>
            <?php foreach ($checkInArrays as $checkIn){?>
                <div class="card col-12 p-4" id="checkins">
                    <h3><?=$checkIn['user_name']; ?></h3>
                    <p class="card-text"><?=$checkIn['rating']; ?></p>
                    <p><?=$checkIn['review']; ?></p>
                    <p><?=$checkIn['submitted']; ?></p>
                    <form action="deleteCheckin.php" method="post" enctype="application/x-www-form-urlencoded">
                        <input name="id" type="hidden" value="<?=$checkIn['id']; ?>">
                        <button type="submit">Delete</button>
                    </form>
                </div>
            <?php } ?>
        </div>
    </div>
</body>
<script>
    // function onSubmit(token) {
    //     document.getElementById("form1").submit();
    // }


    // window.addEventListener('DOMContentLoaded', function() {
    //     var form = $('#form1');
    //     form.on('submit', function(e) {
    //         e.preventDefault();
    //         checkin();
    //     });
    // });
    //
    // function checkin() {
    //     axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    //
    //     var name = $('#Name').val();
    //     var rating = $('#Rating').val();
    //     var review = $('#Review').val();
    //
    //     var data = new FormData();
    //     data.append('name', name);
    //     data.append('rating', rating);
    //     data.append('review', review);
    //
    //     axios.post('http://localhost/project/Week%203/Project/Project%20Week%203.php', data)
    //         .then(function (response) {
    //             console.log(response);
    //             $('#message').removeClass('invisible').text(response.data);
    //         })
    //         .catch(function(error) {
    //             console.log(error);
    //         });
    // }
</script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</html>