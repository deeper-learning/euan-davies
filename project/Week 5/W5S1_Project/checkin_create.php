<?php

require_once 'db.php';

$newCheckinName = 'Euan';

$stmt = $dbh->prepare(
    'INSERT INTO checkins (user_name) VALUES (:user_name)'
);

$stmt->execute([
    'user_name' => $newCheckinName
]);

echo '# Rows affected: ' . $stmt->rowCount();



?>