<?php
    require_once 'db.php';

    if (!empty($_POST)) {

        $stmt = $dbh->prepare(
            'DELETE FROM checkins WHERE id = :id;'
        );

        $stmt->execute([
            'id' => $_POST['id'],
        ]);

        if ($stmt->rowCount() > 0) {
            echo 'Removed Successfully';
        }
        header("Location: Project Week 5.php");
        die();
    }

?>