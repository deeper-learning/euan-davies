CREATE TABLE `products` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(16) NOT NULL,
    `description` VARCHAR (700) NOT NULL, 
    `image_path` VARCHAR(120) NOT NULL, 
    PRIMARY KEY (`id`) 
); 