<?php

// use \App\Entity\Product;

require_once '../src/setup.php';

$stmt = $db->prepare('SELECT id, title FROM product');
$stmt->execute();

$products = $stmt->fetchAll(PDO::FETCH_CLASS, Product::class);

?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <!--    <script src="https://www.google.com/recaptcha/api.js?render=--><?php //echo SITE_KEY; ?><!--"></script>-->
    <!--    <script src="https://www.google.com/recaptcha/api.js"></script>-->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <title>Product Detail</title>
</head>
<body>
<div class="container">
    <h1>Products</h1>
    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($products as $product): ?>
            <tr>
                <td><a href="product.php?productId=<?= $product->id ?>"><?= $product->id ?></a></td>
                <td><?= $product->title ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
</body>
</html>