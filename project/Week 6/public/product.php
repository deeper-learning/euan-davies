<?php

// use App\Entity\Product;
// use App\Entity\CheckIn;

use ReCaptcha\ReCaptcha;

require_once '../src/setup.php';
require_once __DIR__ . '/vendor/autoload.php';

if (isset($_POST['g-recaptcha-response'])) {
    $secret = '6LdM-mQaAAAAACCnrukf4-zB_Ou_smz52nYTZ4OG';
    $gRecaptchaResponse = $_POST['g-recaptcha-response'];
    $remoteIp = $_SERVER['REMOTE_ADDR'];
    //var_dump($remoteIp);
    $recaptcha = new \ReCaptcha\ReCaptcha($secret);
    $resp = $recaptcha->setExpectedHostname('localhost')
        ->verify($gRecaptchaResponse, $remoteIp);
    if ($resp->isSuccess()) {
        // echo 'Verified!';
    } else {
        $errors = $resp->getErrorCodes();
        var_dump($errors);
    }
}

$checkinAddedSuccess = false;

//$stmt = $db->prepare(
//    'SELECT product.id, product.title,
//    AVG(checkins.rating) AS average_rating
//  FROM product
//  LEFT JOIN checkins ON checkins.product_id = product.id
//  GROUP BY product.id'
//);
//
//$stmt->execute([
//    `id` => $productId
//]);


$productId = $_GET['productId'];
$product = new Product();
$product->id = $productId;

$stmt = $db->prepare('SELECT AVG(rating) as averageRating FROM checkins');
$stmt->execute();
$product->average_rating = $stmt->fetch()[0];

$product->title = 'TODO'; //3RD query for title as one join query.

$stmt = $db->prepare('
  SELECT * FROM `checkins` WHERE `product_id` = :productId'
);
$stmt->execute(['productId' => $product->id]);
$checkIns = $stmt->fetchAll();

function hydrateCheckIn(array $data): CheckIn
{
    $checkin = new CheckIn();
    $checkin-> id = $data['id'];
    $checkin-> user_name = $data['user_name'];
    $checkin-> product_id = $data['product_id'];
    $checkin-> rating = $data['rating'];
    $checkin-> review = $data['review'];
    $checkin-> submitted = $data['submitted'];
    return $checkin;
}

foreach ($checkIns as $checkIn) {
    $hydratedCheckIn = hydrateCheckIn($checkIn);
    $product->addCheckIn($hydratedCheckIn);
}


//$hydratedCheckIns = array_map(static function (array $checkin): CheckIn
//{
//    return hydrateCheckIn($checkin);
//}, $checkIns);



?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <!--    <script src="https://www.google.com/recaptcha/api.js?render=--><?php //echo SITE_KEY; ?><!--"></script>-->
    <!--    <script src="https://www.google.com/recaptcha/api.js"></script>-->
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <title>Product Detail</title>
    <style>
        .star-rating {
            background-color: grey;
            width: 200px;
            height: 30px;
            display: inline-block;
        }

        .star-rating div {
            height: 100%;
            background-color: cornflowerblue;

        }
    </style>
</head>
<body>
<div class="row m-4" id="main">
    <div class="row border p-2 pb-4 m-3">
        <div class="col-md-12 col-xl-6">
            <div class="card-body">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="https://www.rover.com/blog/wp-content/uploads/2014/10/tiny-pug1-750x540.jpg" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="https://www.rover.com/blog/wp-content/uploads/2014/10/tiny-pug1-750x540.jpg" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="https://www.rover.com/blog/wp-content/uploads/2014/10/tiny-pug1-750x540.jpg" alt="Third slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="https://www.rover.com/blog/wp-content/uploads/2014/10/tiny-pug1-750x540.jpg" alt="Fourth slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-xl-6">
            <div class="card-body">
                <h1 class="card-title">Lorem Ipsum</h1>
                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#checkinForm">Check In</button>
                <div class="alert alert-success invisible " id="message"></div>
                <div>
                    <div class="modal fade" id="checkinForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form action="" method="post" id="form1">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Submit Your Review</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="checkin-name">Name</label>
                                            <input type="text" class="form-control" name="Name" id="Name" placeholder="Enter Name">
                                        </div>
                                        <div class="form-group">
                                            <label for="checkin-rating">Rating</label>
                                            <select class="form-control" name="Rating" id="Rating">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="checkin-review">Review</label>
                                            <input type="text" class="form-control" name="Review" id="Review" aria-describedby="emailHelp" placeholder="Enter Review">
                                        </div>
                                        <div class="g-recaptcha" data-sitekey="6LdM-mQaAAAAAFeVF1ph_NDJSPLUQkHSgqYZvMQi"></div>
                                        <br/>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <input type="submit" class="btn btn-primary" value="Submit">
                                    </div>
                                </form>
                                <script>

                                    //function onClick(e) {
                                    //    e.preventDefault();
                                    //    grecaptcha.ready(function() {
                                    //        grecaptcha.execute('<?php //echo SITE_KEY; ?>//', {action: 'submit'}).then(function(token) {
                                    //            console.log(token);
                                    //            //document.getElementById('g-recaptcha-response').value=token;
                                    //        });
                                    //    });
                                    //}
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <br>
        <h2 class="col-12">Additional Information</h2>
        <br>
        <div class="card col-12">
            <table class="table">
                <tbody>
                <br>
                <tr>
                    <th scope="row">Average Rating</th>
                    <td><?= $product->average_rating ?></td>
                </tr>
                <tr>
                    <th scope="row">Another Statistic</th>
                    <td>78/100</td>
                </tr>
                <tr>
                    <th scope="row">Yet Another Statistic</th>
                    <td>Something</td>
                </tr>
                </tbody>
            </table>
        </div>
        <br>
    </div>
    <div class="col-12">
        <h2 class="col-12">Recent Checkins</h2>
        <br>
<!--        --><?php //if ($checkinAddedSuccess): ?>
<!--            <p class="alert alert-success">Your Review has been added successfully.</p>-->
<!--        --><?php //endif; ?>
        <?php foreach ($product->getCheckins() as $checkIn){?>
            <div class="card col-12 p-4" id="checkins">
                <h3><?=$checkIn->user_name?></h3>
                <p class="card-text"><?=$checkIn->rating?></p>
                <p><?=$checkIn->review; ?></p>
                <p><?=$checkIn->submitted; ?></p>
                <form action="deleteCheckin.php" method="post" enctype="application/x-www-form-urlencoded">
                    <input name="id" type="hidden" value="<?=$checkIn->id; ?>">
                    <button type="submit">Delete</button>
                </form>
            </div>
        <?php } ?>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
</html>
