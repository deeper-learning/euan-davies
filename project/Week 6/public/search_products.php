<?php
if (!isset($_GET['id'])) {
    die('Please specify an id in the URL.');
}

class Product
{
    public int $id;
    public string $title;
    /** @var CheckIn[] */
    public array $checkIns;
}

class CheckIn
{
    public int $id;
    public int $product_id;
    public string $user_name;
    public int $rating;
    public string $review;
    public string $submitted;
}

$db = new PDO('mysql:host=mysql;dbname=project', 'root', 'root');
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// Product
$stmt = $db->prepare('SELECT * FROM `product` WHERE `id` = :id');
$stmt->execute(['id' => $_GET['id']]);

$product = $stmt->fetchObject(Product::class);

//Checkin
$stmt = $db->prepare('
  SELECT * FROM `checkins` WHERE `product_id` = :product_id'
);
$stmt->execute(['product_id' => $product->id]);
$product->checkIns = $stmt->fetchAll(PDO::FETCH_CLASS, CheckIn::class);
var_dump($product);

?>

