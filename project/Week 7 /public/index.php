<?php

require_once '../src/setup.php';

$stmt = $dbh->prepare('SELECT * FROM films');

$stmt->execute();

//$films = $stmt->fetchObject(films::class);
$films = $stmt->fetchAll(PDO::FETCH_CLASS, films::class);
//var_dump($films);
//$stmt->execute(['filmsId' => $films->id]);

//$film = $stmt->fetchAll();

//$films = new films();
//
//function hydratefilms(array $data): films
//{
//    $films = new films();
//    $films->id = $data['id'];
//    $films->name = $data['name'];
//    return $films;
//}

var_dump($films);

//$stmt = $dbh->prepare('SELECT * FROM films f LEFT JOIN Harry_Potter h ON h.films_id = f.id
//WHERE f.id = :id');

$stmt = $dbh->prepare('SELECT * FROM Harry_Potter');

$stmt->execute();

$harrys = $stmt->fetchAll(PDO::FETCH_CLASS, Harry_Potter::class);

var_dump($harrys);

//function hydrateHarry(array $data): Harry_Potter
//{
//    $harry = new Harry_Potter();
//    $harry->id = $data['id'];
//    $harry->filmsId = $data['films_id'];
//    $harry->quote = $data['quote'];
//    $harry->film = $data['film'];
//    return $harry;
//}

//$stmt->execute(['filmsId' => $films->id]);
//$checkIns = $stmt->fetchAll();
//
//foreach ($harrys as $harry) {
//    $hydratedHarry = hydrateHarry($harry);
//    $films->addHarry($hydratedHarry);
//}

//var_dump($films);
//$stmt = $dbh->prepare('SELECT id, films_id, quote, film FROM Harry_Potter WHERE films_id = :id');
//$stmt->execute(['id' => $_GET['id']]);

//$harrys = $stmt->fetchAll('PDO:FETCH_ASSOC');
//$hydratedHarrys = array_map(static function (array $harry): Harry_Potter {
//    return hydrateHarry($harry);
//}, $harrys);

//var_dump($hydratedHarrys);

//foreach ($film as $films) {
//    $hydratedfilms = hydratefilms($films);
//};

//var_dump($hydratedfilms);

//function hydrateFilmsWithHarry(array $data): films
//{
//    $films = new films();
//    $films->id = $data[0]['product_id'];
//    $films->name = $data[0]['name'];
//
//    foreach ($data as $filmRow){
//        $harry = new Harry_Potter();
//        $harry->id = $filmRow['id'];
//        $harry->filmsId = $filmRow['films_id'];
//        $harry->quote = $filmRow['quote'];
//        $harry->film = $filmRow['film'];
//
//        $films->addHarry($harry);
//    }
//
//    return $films;
//}

//var_dump($films);

//$Harry_Potters = $stmt->fetchAll(PDO::FETCH_CLASS, Harry_Potter::class);

//foreach ($Harry_Potters as $Harry_Potter) {
//    $films->addHarryPotter($Harry_Potter);
//}

//var_dump($films);

?>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="styling.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <title>Quote Generator</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Quote Generator</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="quote_list.php">Quote List</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Log In</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#">Disabled</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="row m-4" id="main">
        <div class="row border p-2 pb-4 m-3">
            <div class="col-md-12 col-xl-6">
                <div class="card-body">
                    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="https://cdn.mos.cms.futurecdn.net/uktxA9dyjsinpknx48duCV.jpg" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="https://images.indianexpress.com/2018/07/wp-harrypotter-01.jpg" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="https://cdn.britannica.com/19/187419-050-94D978BD/Mark-Hamill-Luke-Skywalker-Yoda-Irvin-Kershner.jpg" alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-xl-6">
                <br>
                <h1>Movie Quote Generator!</h1>
                <br>
                <p>Press the button bellow to receive a random
                quote from Lord of the Rings, Harry Potter and Star Wars.</p>
                <button type="button" class="btn btn-primary">Quote</button>
            </div>
        </div>
</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</html>
