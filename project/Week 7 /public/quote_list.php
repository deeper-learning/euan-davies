<?php

// use \App\Entity\Product;

require_once '../src/setup.php';

$stmt = $dbh->prepare('SELECT id, name FROM films');
$stmt->execute();

$films = $stmt->fetchAll(PDO::FETCH_CLASS, films::class);

?>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <title>Product Detail</title>
</head>
<body>
<div class="container">
    <h1>Films List</h1>
    <table class="table">
        <thead>
        <tr>
            <th>Id</th>
            <th>Film Name</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($films as $film): ?>
            <tr>
                <td><a href="index.php?filmId=<?= $film->id ?>"><?= $film->id ?></a></td>
                <td><?= $film->name ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
</body>
</html>
