<?php

$username = 'root';
$password = 'root';

require_once 'Entity/films.php';
require_once 'Entity/Harry_Potter.php';
require_once 'Entity/LOTR.php';
require_once 'Entity/Starwars.php';

try {
    $dbh = new PDO (
        'mysql:dbname=quote;host=mysql',
        $username,
        $password
    );

    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch (PDOException $e) {
    die ('Unable to establish database connection.');
}

?>